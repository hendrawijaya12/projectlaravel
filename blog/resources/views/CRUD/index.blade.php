@extends('layout.master')

@section('judul')
Halaman List Aktor
@endsection

@section('content')

<a href="/cast/create" class="btn btn-success btn-lsm mb-3">Tambah Data</a>

<table class="table table-sm">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama aktor</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->umur}}</td>
                <td>{{$item->bio}}</td>
                <td>
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                    <form action="/cast/{{$item->id}}" method="POST">
                        @method('delete')
                        @csrf

                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                
                <td>Data kosong</td>
            </tr>
        @endforelse
    </tbody>
</table>
@endsection