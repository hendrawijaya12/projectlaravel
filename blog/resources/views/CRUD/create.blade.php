@extends('layout.master')

@section('judul')
Halaman biodata
@endsection

@section('content')

    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label>id</label><br> 
            <input type="text" name="id">
        </div>
        @error('id')
           <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Nama aktor</label><br>
            <input type="text" name="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Umur</label><br>
            <input type="text" name="umur">
        </div>
        @error('film_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Bio</label><br>
            <input type="text" name="bio">
        </div>
        <button type="submit" class="btn btn-primary">Buat</button>
    </form>
@endsection