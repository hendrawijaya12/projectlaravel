@extends('layout.master')

@section('judul')
Halaman Edit {{$cast->nama}}
@endsection

@section('content')

    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label>id</label><br> 
            <input type="text" name="id" value="{{$cast->id}}">
        </div>
        @error('id')
           <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Nama aktor</label><br>
            <input type="text" name="nama" value="{{$cast->nama}}">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Umur</label><br>
            <input type="text" name="umur" value="{{$cast->umur}}">
        </div>
        @error('film_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Bio</label><br>
            <input type="text" name="bio" value="{{$cast->bio}}">
        </div>
        <button type="submit" class="btn btn-primary">Buat</button>
    </form>
@endsection